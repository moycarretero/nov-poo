<?php

class Coche
{
    private string $matricula;
    private string $marca;
    private string $modelo;
    private string $tipoGasolina;
    private float $kilometrosRecorridos;
    private float $cantidadCombustible;

    public function __construct(string $marca, string $modelo)
    {
        $this->marca = $marca;
        $this->modelo = $modelo;

        $this->kilometrosRecorridos = 0;
        $this->cantidadCombustible = 0;
    }

    function repostar(float $litrosCombustible){
        $this->cantidadCombustible += $litrosCombustible;
    }

    function mover(float $kilometros){
        $this->kilometrosRecorridos += $kilometros;

        $this->cantidadCombustible -= $kilometros * 0.05;
    }

    function pasarITV(){

    }

    /**
     * @return string
     */
    public function getMatricula(): string
    {
        return $this->matricula;
    }

    /**
     * @param string $matricula
     */
    public function setMatricula(string $matricula): void
    {
        $this->matricula = $matricula;
    }

    /**
     * @return string
     */
    public function getMarca(): string
    {
        return $this->marca;
    }

    /**
     * @param string $marca
     */
    public function setMarca(string $marca): void
    {
        $this->marca = $marca;
    }

    /**
     * @return string
     */
    public function getModelo(): string
    {
        return $this->modelo;
    }

    /**
     * @param string $modelo
     */
    public function setModelo(string $modelo): void
    {
        $this->modelo = $modelo;
    }

    /**
     * @return string
     */
    public function getTipoGasolina(): string
    {
        return $this->tipoGasolina;
    }

    /**
     * @param string $tipoGasolina
     */
    public function setTipoGasolina(string $tipoGasolina): void
    {
        $this->tipoGasolina = $tipoGasolina;
    }

    /**
     * @return float
     */
    public function getKilometrosRecorridos(): float
    {
        return $this->kilometrosRecorridos;
    }

    /**
     * @param float $kilometrosRecorridos
     */
    public function setKilometrosRecorridos(float $kilometrosRecorridos): void
    {
        $this->kilometrosRecorridos = $kilometrosRecorridos;
    }

    /**
     * @return float
     */
    public function getCantidadCombustible(): float
    {
        return $this->cantidadCombustible;
    }

    /**
     * @param float $cantidadCombustible
     */
    public function setCantidadCombustible(float $cantidadCombustible): void
    {
        $this->cantidadCombustible = $cantidadCombustible;
    }

    function pintarInfo()
    {
        echo "<ul>";
        echo "<li>Marca: {$this->marca}</li>";
        echo "<li>Modelo: {$this->modelo}</li>";
        echo "<li>Kilometros Recorridos: {$this->kilometrosRecorridos}</li>";
        echo "<li>Cantidad Comubisble: {$this->cantidadCombustible}</li>";
        echo "</ul>";
    }


}
