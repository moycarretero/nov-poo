<?php
    require_once "Coche.php";

    $miCoche = new Coche('Ford', 'Focus');

    $miCoche->repostar(20);
    $miCoche->mover(50);
    $miCoche->repostar(5);
    $miCoche->mover(78);

    $miCoche->pintarInfo();

    $miCoche->mover(100);

    $miCoche->pintarInfo();

    $otroCoche = new Coche('Seat', 'Ibiza');

    $otroCoche->repostar(99);
    $otroCoche->mover(543);

    $otroCoche->pintarInfo();

    $cocheCR = new Coche('Ferrari', 'F40');
    $cocheCR->pintarInfo();
